const solutionsData = {
    en: [
        {
            id: "01",
            title: "Issues Identification",
            description: "Identify the main mistakes made when creating a business and bring them in line."
        },
        {
            id: "02",
            title: "Path Determination",
            description: "Determine the company's path in the market."
        },
        {
            id: "03",
            title: "Product Evaluation",
            description: "Determine product value."
        },
        {
            id: "04",
            title: "Product Strategy",
            description: "Create a successful company and product strategy."
        },
        {
            id: "05",
            title: "Dynamic",
            description: "Hella narwhal Cosby sweater McSweeney's, salvia kitsch before they sold out High Life. Umami tattooed sriracha meggings pickled Marfa Blue Bottle High Life next level."
        },
        {
            id: "06",
            title: "Acustomed Web Solutions",
            description: "On behalf of above we will rebrand your face on market place, develop software, web or mobile app design. Promote your brand on all social networks and will do even more."
        }
    ],
    ru: [
        {
            id: "01",
            title: "Issues Identification",
            description: "Identify the main mistakes made when creating a business and bring them in line."
        },
        {
            id: "02",
            title: "Path Determination",
            description: "Determine the company's path in the market."
        },
        {
            id: "03",
            title: "Product Evaluation",
            description: "Determine product value."
        },
        {
            id: "04",
            title: "Product Strategy",
            description: "Create a successful company and product strategy."
        },
        {
            id: "05",
            title: "Dynamic",
            description: "Hella narwhal Cosby sweater McSweeney's, salvia kitsch before they sold out High Life. Umami tattooed sriracha meggings pickled Marfa Blue Bottle High Life next level."
        },
        {
            id: "06",
            title: "Acustomed Web Solutions",
            description: "On behalf of above we will rebrand your face on market place, develop software, web or mobile app design. Promote your brand on all social networks and will do even more."
        }
    ]
}

export default function Solutions (lang){
    if (!lang) {
        lang = process.env.VUE_APP_I18N_LOCALE;
    }

    return {
        getData: () =>{
            return solutionsData[lang];
        }
    }
}
