const langArray = [
    {
        id: 'en',
        title: 'English'
    },
    {
        id: 'ru',
        title: 'Russian'
    }
];

export default function Languages() {
    return {
        getKeyLanguages: () => {
            return langArray.reduce((acc, item) => [...acc, item.id], []);
        },
        getLanguages: () => {
            return langArray;
        }
    }
}
