const plans = [
    {
        id: 1,
        name: 'Starter',
        custom: null,
        for: 'For Small Business',
        smallFor: 'Small Business',
        chosen: false,
        prices: {
            single: ['1', '000'],
            subscription: ['1', '300']
        },
        save: 18
    },
    {
        id: 2,
        name: 'Super',
        custom: null,
        for: 'For Medium Business',
        smallFor: 'Medium Business',
        chosen: false,
        prices: {
            single: ['2', '000'],
            subscription: ['2', '300']
        },
        save: 22
    },
    {
        id: 3,
        name: 'Advanced',
        custom: null,
        for: 'Popular Choice',
        smallFor: 'Popular Choice',
        chosen: true,
        prices: {
            single: ['3', '000'],
            subscription: ['3', '300']
        },
        save: 26
    },
    {
        id: 4,
        name: 'Pro',
        custom: {
            title: 'Custom Plans'
        },
        for: 'For Growing Business',
        smallFor: 'Growing Business',
        chosen: false,
        prices: {},
        save: 30
    }
];
const sections = [
    {
        icon: 'key',
        name: 'Defining new strategy',
        plans: [1, 2, 3, 4],
        items: [
            {name: 'Marketing Strategy Analysis', plans: [1, 2, 3, 4]},
            {name: 'Fund Analysis', plans: [2, 3, 4]},
            {name: 'Finance Analysis', plans: [3, 4]},
            {name: 'Supply Chain Analysis', plans: [3, 4]},
            {name: 'On-Site Business Support', plans: [4]}
        ]
    },
    {
        icon: 'key',
        name: 'Crisis Management',
        plans: [2, 3, 4],
        items: [
            {name: 'Marketing Strategy Analysis', plans: [1, 2, 3, 4]},
            {name: 'Fund Analysis', plans: [2, 3, 4]},
            {name: 'Finance Analysis', plans: [3, 4]},
            {name: 'Supply Chain Analysis', plans: [3, 4]},
            {name: 'On-Site Business Support', plans: [4]}
        ]
    },
    {
        icon: 'key',
        name: 'Vertical solutions',
        plans: [2, 3, 4],
        items: [
            {name: 'Marketing Strategy Analysis', plans: [1, 2, 3, 4]},
            {name: 'Fund Analysis', plans: [2, 3, 4]},
            {name: 'Finance Analysis', plans: [3, 4]},
            {name: 'Supply Chain Analysis', plans: [3, 4]},
            {name: 'On-Site Business Support', plans: [4]}
        ]
    },
    {
        icon: 'key',
        name: 'Marketing optimization',
        plans: [3, 4],
        items: [
            {name: 'Marketing Strategy Analysis', plans: [1, 2, 3, 4]},
            {name: 'Fund Analysis', plans: [1, 2, 3, 4]},
            {name: 'Finance Analysis', plans: [3, 4]},
            {name: 'Supply Chain Analysis', plans: [3, 4]},
            {name: 'On-Site Business Support', plans: [4]}
        ]
    },
    {
        icon: 'key',
        name: 'On-Site Support',
        plans: [3, 4],
        items: [
            {name: 'Marketing Strategy Analysis', plans: [2, 3, 4]},
            {name: 'Fund Analysis', plans: [2, 3, 4]},
            {name: 'Finance Analysis', plans: [3, 4]},
            {name: 'Supply Chain Analysis', plans: [3, 4]},
            {name: 'On-Site Business Support', plans: [4]}
        ]
    },
    {
        icon: 'key',
        name: 'On-Site Support',
        plans: [4],
        items: [
            {name: 'Marketing Strategy Analysis', plans: [2, 3, 4]},
            {name: 'Fund Analysis', plans: [2, 3, 4]},
            {name: 'Finance Analysis', plans: [3, 4]},
            {name: 'Supply Chain Analysis', plans: [3, 4]},
            {name: 'On-Site Business Support', plans: [4]}
        ]
    },
    {
        icon: 'plus',
        name: 'Custom Web Solutions',
        plans: [1, 2, 3, 4],
        items: [
            {name: 'Branding & Identity', plans: [1, 2, 3, 4],},
            {name: 'UX Consulting', plans: [1, 2, 3, 4],},
            {name: 'Mobile & Web App Development', plans: [1, 2, 3, 4],},
            {name: 'Custom Website Development', plans: [1, 2, 3, 4],},
            {name: 'Digital Marketing', plans: [1, 2, 3, 4],}
        ]
    }
]
const plansData = {
    en: {
        plans,
        sections
    },
    ru: {
        plans,
        sections
    }
}

export default function Plans(lang) {
    if (!lang) {
        lang = process.env.VUE_APP_I18N_LOCALE;
    }

    return {
        getData: () => {
            return plansData[lang];
        },
        planById: (pkgId) => {
            return plans.find(({id}) => id === parseInt(pkgId));
        }
    }
}
