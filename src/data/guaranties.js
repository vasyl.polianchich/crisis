const guarantiesData = {
    en: [
        {
            id: "01",
            title: "Profit Increase",
            description: "A profit increase of more than 3 times, while implementing all the strategic steps proposed by us. Some of the steps can be implemented at the same time to have a larger effect on profit."
        },
        {
            id: "02",
            title: "Business Support",
            description: "After the start of the project, we provide support for 6 months. At the same time, we are helping companies, products and businesses to stay lean and competitive on an individually tailored plan."
        },
        {
            id: "03",
            title: "Income Increase",
            description: "We are helping to increase profitability as a result of creating customized solutions on behalf of well-tailored re-invented marketing and web strategies to meet consumer expectations."
        },
    ],
    ru: [
        {
            id: "01",
            title: "Profit Increase",
            description: "A profit increase of more than 3 times, while implementing all the strategic steps proposed by us. Some of the steps can be implemented at the same time to have a larger effect on profit."
        },
        {
            id: "02",
            title: "Business Support",
            description: "After the start of the project, we provide support for 6 months. At the same time, we are helping companies, products and businesses to stay lean and competitive on an individually tailored plan."
        },
        {
            id: "03",
            title: "Income Increase",
            description: "We are helping to increase profitability as a result of creating customized solutions on behalf of well-tailored re-invented marketing and web strategies to meet consumer expectations."
        },
    ]
}

export default function Guaranties(lang) {
    if (!lang) {
        lang = process.env.VUE_APP_I18N_LOCALE;
    }

    return {
        getData: () => {
            return guarantiesData[lang];
        }
    }
}
