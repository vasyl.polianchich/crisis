import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isSingle: true,
    addressBilling: {},
    timeoutOffer: null
  },
  mutations: {
    setSingle: (state, payload) => {
      state.isSingle = payload;
    },
    setAddress: (state, payload) => {
      state.addressBilling = payload;
    },
    setTimeoutOffer: (state, payload) => {
      state.timeoutOffer = payload;
    }
  },
  actions: {
    setSingle: ({commit}, payload) => {
      commit('setSingle', payload);
    },
    setAddress: ({commit}, payload) => {
      commit('setAddress', payload);
    },
    setTimeoutOffer: ({commit}, payload) => {
      commit('setTimeoutOffer', payload);
    },
  },
  getters: {
    isSingle: ({isSingle}) => isSingle,
    addressBilling: ({addressBilling}) => addressBilling,
    timeoutOffer: ({timeoutOffer}) => timeoutOffer,
  },
  modules: {}
});
