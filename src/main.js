import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./i18n";
import VueCardFormat from "vue-credit-card-validation";
import VueScrollactive from 'vue-scrollactive';
import VueScrollTo from 'vue-scrollto';
import TextareaAutosize from 'vue-textarea-autosize';
import Ripple from 'vue-ripple-directive';
import VModal from 'vue-js-modal';
import Affix from 'vue-affix';
import VueMask from 'v-mask';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VTooltip from 'v-tooltip';

Vue.use(VTooltip);
Vue.use(VueAxios, axios);
Vue.use(VueMask);
Vue.use(Affix);
Vue.use(VModal, { dynamic: true, dynamicDefaults: { clickToClose: false } })
Vue.directive('ripple', Ripple);
Vue.use(TextareaAutosize)
Vue.use(VueScrollactive);
Vue.use(VueCardFormat);
Vue.use(VueScrollTo, {
  container: "body",
  duration: 50,
  easing: "ease",
  offset: -50,
  force: true,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
})

  Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
