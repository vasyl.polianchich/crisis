import Plans from "../data/plans";
import { mapGetters } from 'vuex';

export const packageMixin = {
    data: () => {
        return {
            plansData: Plans("en").getData()
        }
    },
    methods: {
        selectPlan(pkgId) {
            if (this.timeoutOffer) {
                clearTimeout(this.timeoutOffer);
            }
            this.$modal.hide('plan');
            this.$router.push({ name: 'Checkout', params: { pkgId } });
        },
        onChange: function () {
            this.$store.dispatch('setSingle', !this.isSingle)
        },
        getImgUrl: (icon) => {
            return require(`../assets/img/icons/${icon}.svg`)
        },
        disableSection: function (plans, planId) {
            return !plans.includes(planId);
        }
    },
    computed: {
        getPlanPrice: function () {
            return this.isSingle ? 'single' : 'subscription';
        },
        ...mapGetters([
            'isSingle',
            'timeoutOffer'
        ])
    }
}
